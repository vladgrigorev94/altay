<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

$listRent = [
    [
        'slug' => 'kia-rio',
        'name' => 'Kia Rio (белый)',
        'description' => 'Аренда Kia Rio в Горно-Алтайске',
        'main_img_url' => '/img/cars/Kia-Rio-2016-foto.jpg',
        'styles_href' => '',
        'styles_image' => '',
        'specifications' => [
            'year' => 2018,
            'carcase' => 'седан',
            'engine_volume' => 1.6,
            'drive_unit' => 'передний',
            'transmission' => 'автомат',
            'fuel_consumption' => '10 л/100 км',
            'engine_power' => '120 л/с',
            'color' => 'белый',
            'fuel_type' => 'АИ-92',
            'cost' => 'от 2500 руб/сутки'
        ]
    ],
    [
        'slug' => 'renault-logan',
        'name' => 'Renault Logan (металик)',
        'description' => 'Аренда Renault Logan в Горно-Алтайске',
        'main_img_url' => '/img/cars/renault.jpg',
        'styles_href' => 'overflow: hidden;height: 400px;display: block;',
        'styles_image' => 'margin-top: -130px;',
        'specifications' => [
            'year' => 2012,
            'carcase' => 'седан',
            'engine_volume' => 1.4,
            'drive_unit' => 'передний',
            'transmission' => 'механическая',
            'fuel_consumption' => '8 л/100 км',
            'engine_power' => '70 л/с',
            'color' => 'металик',
            'fuel_type' => 'АИ-92',
            'cost' => 'от 1500 руб/сутки'
        ]
    ],
];

Route::get('/', function () use ($listRent) {
    return view('about.about', [
        'list' => $listRent
    ]);
})->name('about');

Route::post('/', function (Request $req) {
    $auto = new \App\Models\Auto();

    $auto->name = $req->name;
    $auto->email = $req->email;
    $auto->phone = $req->phone;

    foreach ($req->theme as $index => $item) {
        if ($index != 0) {
            $auto->theme .= ', ' . $item;
        }
    }

    foreach ($req->model as $index => $item) {
        if ($index != 0) {
            $auto->model .= ', ' . $item;
        }
    }

    $auto->date_from = $req->date_from;
    $auto->date_till = $req->date_till;
    $auto->comment = $req->comment;

    $auto->save();

    return redirect('/');
});

Route::get('/rent-auto', function () use ($listRent) {
    return view('rents.list', [
        'list' => $listRent
    ]);
})->name('rent.index');

Route::get('/rent-view/{slug}', function ($slug) use ($listRent) {
    $indexAuto = array_search($slug, array_column($listRent, 'slug'));

    if ($indexAuto === false) {
        abort(404);
    }

    return view('detail-view', [
        'item' => $listRent[$indexAuto]
    ]);
})->name('rent-view');

Route::get('/prices', function () use ($listRent) {
    return view('prices', [
        'list' => $listRent
    ]);
})->name('prices');

Route::get('/rent-conditions', function () {
    return view('rent-conditions', []);
})->name('rent-conditions');

Route::get('/contacts', function () use ($listRent) {
    return view('contacts', [
        'list' => $listRent
    ]);
})->name('contacts');
