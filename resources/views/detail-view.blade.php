@extends('layouts.default')

@section('title', 'Автомобиль ' . $item['name'] . ' | ' . config('app.site_title')  . ' | '. config('app.name'))

@section('description', 'Автомобиль ' . $item['name'] . '. Описание нашего авто для проката/аренды в Республике Алтай. '. config('app.name'))

@section('content')
    <div class=" bd-content-7">
        <div class=" bd-blog-5 bd-page-width  " itemscope="" itemtype="http://schema.org/Article">
            <div class="bd-container-inner">
                <div class=" bd-grid-7 bd-margins">
                    <div class="container-fluid">
                        <div class="separated-grid row">
                            <div class="separated-item-46 col-md-12 first-col last-row last-col"
                                 style="overflow: visible; height: auto;">
                                <div class="bd-griditem-46">
                                    <article class=" bd-article-4" style="position: relative;">
                                        <div class=" bd-pagetitle-3 bd-background-width ">
                                            <div class="bd-container-inner">
                                                <h1>
                                                    Аренда авто - Прокат автомобилей в Горном Алтае
                                                </h1>
                                            </div>
                                        </div>

                                        <div class=" bd-postcontent-4 bd-tagstyles bd-contentlayout-offset"
                                             itemprop="articleBody">
                                            <p><!--[html]-->
                                                <style></style>
                                            </p>
                                            <div class="bd-tagstyles  additional-class-1793154642 ">
                                                <div class="bd-container-inner bd-content-element"><!--{content}-->
                                                    <p></p>
                                                    <p>
                                                        <strong>Название автомобиля:&nbsp;</strong>
                                                        {{$item['name']}}
                                                    </p>
                                                    <p>
                                                        <strong>
                                                            <img
                                                                src="{{config('app.url') . $item['main_img_url']}}"
                                                                alt="5d692a68424805b12d09e055d10dfcbe"
                                                                width="400"
                                                                style="float: left;"
                                                            >
                                                        </strong>
                                                    </p>

                                                    @include('rents.list-item.specifications', ['item' => $item])

                                                    <p>
                                                        <img
                                                            src="{{config('app.url')}}/img/gorizontal-razdelitel.jpg"
                                                            alt="gorizontal razdelitel"
                                                            width="100%" height="2"
                                                        >
                                                    </p>

                                                    <table style="width: 100%;">
                                                        <tbody>
                                                        <tr>
                                                            <td>
                                                                <p><strong>ОПИСАНИЕ</strong></p>
                                                                <p>Больше динамики и комфорта для тех, кто не боится
                                                                    перемен. Все, что Вы ждете от автомобиля, воплощено
                                                                    в {{$item['name']}} Вобравшая в себя дух природы, новая
                                                                    концепция дизайна является истинным
                                                                    воплощением вашего стиля на дороге. Каждая деталь
                                                                    интерьера нацелена на создание максимального
                                                                    комфорта для водителя и пассажиров. Этот автомобиль
                                                                    располагает к путешествиям. Каждый километр, каждый
                                                                    поворот приносят удовольствие благодаря комфортному,
                                                                    плавному управлению автомобилем. Четко отлаженные
                                                                    настройки и мощные двигатели позволят Вам испытывать
                                                                    удовольствие при вождении в любых ситуациях и при
                                                                    любой погоде.</p>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;
                                                                <p><strong>КОМПЛЕКТАЦИЯ</strong></p>
                                                                <p>Пульт управления центральным замком в ключе<br> •
                                                                    Центральный замок<br> • Фары: Галогеновые<br> •
                                                                    Передние противотуманные фары<br> • Светодиодные
                                                                    дневные ходовые огни<br> • Обогрев внешних зеркал
                                                                    заднего вида<br> • Подогрев передних сидений с тремя
                                                                    режимами<br> • Подогрев рулевого колеса<br> •
                                                                    Электропривод внешних зеркал заднего вида<br> •
                                                                    Электростеклоподъемники передних и задних дверей<br>
                                                                    • Электроусилитель рулевого управления<br> •
                                                                    Регулировка руля по высоте и по вылету<br> •
                                                                    Управление магнитолой на руле<br> •
                                                                    Климат-контроль<br> • Аудиосистема (Радио, 4
                                                                    динамика)<br> • Складывающаяся спинка второго ряда
                                                                    сидений (в пропорции 60:40)<br> • Маршрутный
                                                                    компьютер<br> • Фары проекционного типа со
                                                                    статичными лампами подсветки поворота при повороте
                                                                    руля<br> • Две 12v розетки на центральной
                                                                    консоли<br> • Bluetooth<br> • Разъемы USB и AUX<br>
                                                                    • Регулировка сиденья водителя по высоте</p>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                    <p>
                                                    </p>

                                                    <p style="text-align: center;">
                                                        <a href="{{route('contacts', [])}}">
                                                            <img
                                                                src="{{config('app.url')}}/img/button-bron.png"
                                                                alt="button bron" width="400">
                                                        </a>
                                                    </p>
                                                    <p><!--{/content}--></p>
                                                </div>
                                            </div><!--[/html]--><p></p></div>
                                    </article>
                                    <div style="height:0.4375px" class="bd-empty-grid-item"></div>
                                    <div class="bd-container-inner" style="position: relative;">
                                        <div class="bd-containereffect-3 container-effect container "></div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
