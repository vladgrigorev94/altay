<header class=" bd-headerarea-1 bd-margins">
    <section class=" bd-section-3 bd-tagstyles" id="section3" data-section-title="Simple Travel Header">
        <div class="bd-container-inner bd-margins clearfix">
            <a class="bd-logo-2" href="/">
                <div class="bd-logo-2__inside">
                    <img class="bd-logo-2__image"
                         src="{{asset('icons/logo.svg')}}"
                         alt="{{config('app.name')}} - Аренда автомобилей в Горно-Алтайске">
                    <div class="bd-logo-2__text">{{config('app.name')}}</div>
                </div>
            </a>
            <nav class="bd-hmenu-1" data-responsive-menu="true" data-responsive-levels="expand on click"
                 data-responsive-type="" data-offcanvas-delay="0ms" data-offcanvas-duration="700ms"
                 data-offcanvas-timing-function="ease">

                <div class="bd-menuoverlay-12 bd-menu-overlay"></div>
                <div class="bd-responsivemenu-11 collapse-button">
                    <div class="bd-container-inner">
                        <div class="bd-menuitem-10 ">
                            <a data-toggle="collapse" data-target=".bd-hmenu-1 .collapse-button + .navbar-collapse"
                               href="#" onclick="return false;">
                                <span>ГЛАВНОЕ МЕНЮ САЙТА</span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="navbar-collapse collapse ">
                    <div class=" bd-horizontalmenu-48 clearfix">
                        <div class="bd-container-inner">
                            <ul class=" bd-menu-42 nav nav-center nav-pills">
                                <li class="bd-menuitem-22 bd-toplevel-item item-111 {{ (request()->is('/')) ? 'current' : '' }}">
                                    <a class="{{ (request()->is('/')) ? 'active' : '' }}" href="{{route('about', [])}}">
                                        <span>
                                            О нас
                                        </span>
                                    </a>
                                </li>
                                <li class="bd-menuitem-22 bd-toplevel-item item-112 {{ (request()->is('rent-auto')) ? 'current' : '' }}"
                                ">
                                <a class="{{ (request()->is('rent-auto')) ? 'active' : '' }}"
                                   href="{{route('rent.index', [])}}">
                                        <span>
                                            Аренда авто
                                        </span>
                                </a>
                                </li>
                                <li class=" bd-menuitem-22 bd-toplevel-item item-113 {{ (request()->is('prices')) ? 'current' : '' }}">
                                    <a class="{{ (request()->is('prices')) ? 'active' : '' }}"
                                       href="{{route('prices', [])}}">
                                        <span>
                                            Цены
                                        </span>
                                    </a>
                                </li>
                                <li class=" bd-menuitem-22 bd-toplevel-item item-114 {{ (request()->is('rent-conditions')) ? 'current' : '' }}">
                                    <a class="{{ (request()->is('rent-conditions')) ? 'active' : '' }}"
                                       href="{{route('rent-conditions', [])}}">
                                        <span>
                                            Условия проката
                                        </span>
                                    </a>
                                </li>
                                <li class=" bd-menuitem-22 bd-toplevel-item item-115 {{ (request()->is('contacts')) ? 'current' : '' }}">
                                    <a class="{{ (request()->is('contacts')) ? 'active' : '' }}"
                                       href="{{route('contacts', [])}}">
                                        <span>
                                            Контакты
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="bd-menu-close-icon">
                        <a href="#" class="bd-icon  bd-icon-42"></a>
                    </div>
                </div>
            </nav>

        </div>
    </section>
</header>
