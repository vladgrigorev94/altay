<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" itemprop="blogPost" itemscope=""
     itemtype="http://schema.org/BlogPosting">
    <div class="bd-griditem-30">
        <article class=" bd-article-2">
            <h2 class=" bd-postheader-2" itemprop="name">
                <a href="{{route('rent-view', ['slug' => $item['slug']])}}">
                    {{$item['name']}} - {{$item['description']}}
                </a>
            </h2>
            <div class=" bd-layoutbox-4 bd-no-margins clearfix">
                <div class="bd-container-inner">
                    <div
                        class=" bd-postcontent-2 bd-tagstyles bd-custom-blockquotes bd-custom-bulletlist bd-custom-orderedlist bd-custom-table bd-contentlayout-offset"
                        itemprop="articleBody">
                        <p></p>
                        <div class="bd-tagstyles  additional-class-1023989862 ">
                            <div class="bd-container-inner bd-content-element">
                            </div>
                        </div>
                        <p></p>
                        <p>
                            <strong>Название автомобиля:&nbsp;</strong>
                            {{$item['name']}}
                        </p>
                        <p>
                            <strong>
                                <img
                                    src="{{config('app.url') . $item['main_img_url']}}"
                                    alt="5d692a68424805b12d09e055d10dfcbe"
                                    width="400"
                                    style="float: left;"
                                >
                            </strong>
                        </p>

                        @include('rents.list-item.specifications', ['item' => $item])

                        <p>
                            <img
                                src="{{config('app.url')}}/img/gorizontal-razdelitel.jpg"
                                alt="gorizontal razdelitel"
                                width="100%" height="2"
                            >
                        </p>
                    </div>
                    <a
                        class="bd-postreadmore-1 bd-button "
                        href="{{route('rent-view', ['slug' => $item['slug']])}}"
                    >
                        Подробнее...
                    </a>
                </div>
            </div>
        </article>
        <div class="bd-container-inner"></div>
    </div>
</div>
